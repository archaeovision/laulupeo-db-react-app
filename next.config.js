module.exports = {

    env: {

        Peod: 'https://db.laulupidu.archaeovision.eu/laulupidu-db/items/syndmus',
        Rollid: 'https://db.laulupidu.archaeovision.eu/laulupidu-db/items/roll',
        LoojaRollis: 'https://db.laulupidu.archaeovision.eu/laulupidu-db/items/looja_rollis',
        Looja: 'https://db.laulupidu.archaeovision.eu/laulupidu-db/items/looja'

    },


    webpack: (config, {dev}) => {
        config.module.rules.push(
            {
                test: /\.css$/,
                loader: 'babel-loader!raw-loader!sass-loader'
            },
            {
                test: /\.scss$/,
                loader: 'babel-loader!raw-loader!sass-loader'
            },
            {
                test: /\.svg$/,
                use: ['@svgr/webpack'],
            }
        )
        return config
    }
}
