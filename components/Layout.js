import React from 'react'
import Head from 'next/head'
import Styles from "../css/style.css";
import Sidebar from "./Sidebar";
import NProgress from 'nprogress';
import Router from 'next/router';


Router.events.on('routeChangeStart', url => {
    console.log(`Loading: ${url}`)
    NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

const Layout = props => (
    <React.Fragment>
        <Head>
            <meta charSet="utf-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
            <link
                rel="stylesheet"
                href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
                crossorigin="anonymous"
            />
            <link rel="stylesheet" type="text/css" href="/nprogress.css"/>
            <style dangerouslySetInnerHTML={{__html: Styles}}/>
            <title>Laulupeod</title>
        </Head>
        <div>
            <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
                <a className="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Laulu- ja tantsupeod</a>
            </nav>
            <div className="container-fluid">
                <div className="row">
                    <Sidebar/>
                    <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                        {props.children}
                    </main>
                </div>
            </div>
        </div>
    </React.Fragment>
);

export default Layout;
