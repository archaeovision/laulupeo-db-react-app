import React from 'react'
import shortid from "shortid";



const Sidebar = props => (
    <React.Fragment>
                <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                    <div className="sidebar-sticky">
                        <ul className="nav flex-column">
                            <li className="nav-item" key={shortid.generate()}>
                                <a className="nav-link" href="/">
                                    Sündmus <span></span>
                                </a>
                            </li>
                            <li className="nav-item" key={shortid.generate()}>
                                <a className="nav-link" href="/loojad">
                                    Loojad
                                </a>
                            </li>

                        </ul>
                    </div>
                </nav>
    </React.Fragment>
);

export default Sidebar;
