import Layout from "../components/Layout";
import fetch from 'isomorphic-unfetch';
import Link from "next/link";
import 'bootstrap/dist/css/bootstrap.min.css';
import shortid from "shortid";

const Loojad = props =>  (
    <Layout>
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1>Loojad</h1>
        </div>
      <div className="table-responsive">
            <table className="table table-striped table-sm" id="laulupeod">
                <thead>
                <tr key={shortid.generate()}>
                    <th>Nimi</th>
                    <th>Eesnimi</th>
                    <th>Perekonnanimi</th>
                    <th>Normeeritud</th>
                    <th>Kommentaar</th>
                </tr>
                </thead>
                <tbody>
                {props.unique_posts.map(item =>
                  (
                            <tr key={shortid.generate()}>
                                <td><Link
                                    as={`loojad/${item.id}`}
                                    href="loojad/[looja]"
                                ><a>{item.nimi}</a></Link></td>
                                <td><Link
                                    as={`loojad/${item.id}`}
                                    href="loojad/[looja]"
                                ><a>{item.eesnimi}</a></Link></td>
                                <td>{item.perekonnanimi}</td>
                                <td>{item.norm_nimekuju}</td>
                                <td>{item.kommentaar}</td>
                            </tr>)
                )}
                </tbody>
            </table>
        </div>
    </Layout>
);

Loojad.getInitialProps = async function (context) {
    const res = await fetch(`${process.env.Looja}?meta=total_count%2Cresult_count&sort=nimi&limit=2000&fields=*.*.*`);
    const posts = await res.json();
    const postData = posts.data;
    const unique_posts = postData.filter(function (item) {
        return item.norm_nimekuju_id.length == 0
    })


    return {unique_posts};
};

export default Loojad
