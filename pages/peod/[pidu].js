import Layout from "../../components/Layout";
import fetch from 'isomorphic-unfetch';
import shortid from "shortid";
import Link from "next/link";

function groupBy(OurArray, property) {
    return OurArray.reduce(function (accumulator, object) {
        // get the value of our object(age in our case) to use for group    the array as the array key
        const key = object[property];
        // if the current value is similar to the key(age) don't accumulate the transformed array and leave it empty
        if (!accumulator[key]) {
            accumulator[key] = [];
        }
// add the value to the array
        accumulator[key].push(object);
        // return the transformed array
        return accumulator;
// Also we also set the initial value of reduce() to an empty object
    }, {});
}


const Pidu = props => (
    <Layout>
        <div
            className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 id='title'>{props.laulupidu.pealkiri}</h1>
        </div>

        <div>

            {Object.keys(props.esitused).map(kontsert => (
                <React.Fragment>
                    <h2 key={shortid.generate()}>{kontsert}</h2>
                    <ul>
                        {Object.keys(props.esitused[kontsert]).map(esitus => {
                            return (
                                <React.Fragment>
                                    <h6 key={shortid.generate()}>{esitus}</h6>
                                    {props.esitused[kontsert][esitus].map(looja => {
                                            return (
                                                <React.Fragment>
                                                    <ul>
                                                        {looja.looja ?
                                                            <li key={shortid.generate()}>
                                                                <b> {looja.roll}</b>: <Link
                                                                as={`/loojad/${looja.looja.id}`}
                                                                href="/loojad/[looja]"
                                                            ><a key={shortid.generate()}>{looja.looja.nimi}</a></Link>

                                                            </li>

                                                            : <li key={shortid.generate()}>Vigane kirje: kontrolli
                                                                andmebaasi</li>}
                                                    </ul>
                                                </React.Fragment>

                                            )
                                        }
                                    )

                                    }
                                </React.Fragment>
                            )
                        })}

                    </ul>
                </React.Fragment>
            ))}

        </div>
    </Layout>
);


Pidu.getInitialProps = async function (context) {
    const cont = context.query;
    const res2 = await fetch(`${process.env.LoojaRollis}?meta=total_count%2Cresult_count&limit=200&offset=0&sort=syndmus_id&fields=*.*.*.*&filter%5Bsyndmus_id.ylemsyndmus_id.ylemsyndmus_id%5D%5Beq%5D=${cont.pidu}`);
    const pidu = await res2.json();
    const laulupidu = pidu.data[0].syndmus_id.ylemsyndmus_id.ylemsyndmus_id;

    const sorted_pidu = pidu.data.map(item => (
        {
            kontsert: item.syndmus_id.ylemsyndmus_id.pealkiri,
            esitus: item.syndmus_id.pealkiri,
            roll: item.roll_id.nimi,
            looja: item.looja_id
        }

    ))


    const esitused = groupBy(sorted_pidu, "kontsert");
    const proov_e = Object.keys(esitused).map(item => {
        let a = groupBy(esitused[item], "esitus");
        esitused[item] = a;

    });


    return {laulupidu, esitused};
};

export default Pidu;
