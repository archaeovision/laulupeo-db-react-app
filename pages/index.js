import Layout from "../components/Layout";
import fetch from 'isomorphic-unfetch';
import Link from "next/link";
import 'bootstrap/dist/css/bootstrap.min.css';
import shortid from "shortid";

const Peod = props =>  (
        <Layout>
            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1>Peod</h1>
            </div>
            <div className="table-responsive">
                <table className="table table-striped table-sm" id="laulupeod">
                    <thead>
                        <tr key={shortid.generate()}>
                            <th>Pealkiri</th>
                            <th>Alternatiivne pealkiri</th>
                            <th>Algus</th>
                            <th>Lõpp</th>
                            <th>Koht</th>
                            </tr>
                    </thead>
                    <tbody>
                        {props.posts.data.map(item => {
                            if( item.ylemsyndmus_id === null)
                                return(
                                    <tr key={shortid.generate()}>
                                       <td><Link
                                           as={`peod/${item.id}`}
                                           href="peod/[pidu]"
                                       ><a>{item.pealkiri}</a></Link></td>
                                       <td>{item.alternatiivne_peakiri}</td>
                                       <td>{item.aeg_algus}</td>
                                       <td>{item.aeg_lopp}</td>
                                       <td>{item.asukoht}</td>
                                   </tr>)
                        })}
                    </tbody>
                </table>
            </div>
        </Layout>
);

Peod.getInitialProps = async function (context) {
    const res = await fetch(`${process.env.Peod}?sort=aeg_algus`);
    const posts = await res.json();
    return {posts};
};

export default Peod;
