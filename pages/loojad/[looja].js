import Layout from "../../components/Layout";
import fetch from 'isomorphic-unfetch';
import shortid from "shortid";
import Link from "next/link";
import React from "react";


const Looja = props => (
    <Layout>
        <div
            className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
           <h1>{props.loojaData.data[0].nimi ? props.loojaData.data[0].nimi : `${props.loojaData.data[0].eesnimi} ${props.loojaData.data[0].perekonnanimi}`}</h1>
            <ul>
            {props.nimeList.map(item => (<li key={shortid.generate()}>{item}</li>))}
            </ul>
        </div>
        <div className="table-responsive">
            <table className="table table-striped table-sm">
                <thead>
                <tr key={shortid.generate()}>
                    <th>Roll</th>
                    <th>Esitus</th>
                    <th>Kontsert</th>
                    <th>Pidu</th>
                </tr>
                </thead>
                <tbody>

                {props.loojaRollis.data.map(item => {
                        if (item.roll_id && item.syndmus_id) {
                            return (
                                <tr key={shortid.generate()}>
                                    <td><b> {item.roll_id.nimi}</b></td>
                                    <td>{item.syndmus_id.pealkiri}</td>
                                    <td>{item.syndmus_id.ylemsyndmus_id.pealkiri}</td>
                                    <td><Link as={`/peod/${item.syndmus_id.ylemsyndmus_id.ylemsyndmus_id.id}`}
                                          href="/peod/[pidu]"
                                    ><a>{item.syndmus_id.ylemsyndmus_id.ylemsyndmus_id.pealkiri}</a></Link></td>

                                </tr>
                            )

                        }

                    }
                )}
                </tbody>
            </table>
        </div>
    </Layout>
);


Looja.getInitialProps = async function (context) {
    let querystring = "?meta=total_count%2Cresult_count&limit=200&offset=0&fields=*.*.*.*&filter%5Blooja_id%5D%5Beq%5D="
    const cont = context.query;
    const loojaRes = await fetch(`${process.env.Looja}?meta=total_count%2Cresult_count&limit=200&offset=0&fields=*.*&filter%5Bid%5D%5Beq%5D=${cont.looja}`);
    let loojaData = await loojaRes.json();

    if(loojaData.data[0].norm_nimekuju_id[0] && loojaData.data[0].norm_nimekuju_id[0].looja_rel_id) {
        const loojaRes2 = await fetch(`${process.env.Looja}?meta=total_count%2Cresult_count&limit=200&offset=0&fields=*.*&filter%5Bid%5D%5Beq%5D=${loojaData.data[0].norm_nimekuju_id[0].looja_rel_id}`);
        loojaData = await loojaRes2.json();
    }
    const nimeList = [];

    if(loojaData.data[0].id) {
        const loojaListRes = await fetch(`${process.env.Looja}?meta=total_count%2Cresult_count&limit=2000&offset=0&fields=*.*`);
        const loojalist = await loojaListRes.json();
        const loojalistData = loojalist.data;
        var loojaVariatsioonid = loojalistData.filter(function (item) {
            return item.norm_nimekuju_id[0] && item.norm_nimekuju_id[0].looja_rel_id == loojaData.data[0].id;
        });


        for (let a = 0; a < loojaVariatsioonid.length; a++) {
            querystring+= `${loojaVariatsioonid[a].id},`

                if(loojaVariatsioonid[a].nimi){
                    nimeList.push(loojaVariatsioonid[a].nimi)
                }
                else{
                    let nimi = `${loojaVariatsioonid[a].eesnimi} ${loojaVariatsioonid[a].perekonnanimi} `
                    nimeList.push(nimi);
                }


        }



    }
    else {
        querystring += `${cont.looja},`
    }



    const res = await fetch(`${process.env.LoojaRollis}${querystring}`);
    const loojaRollis = await res.json();
    return {loojaData, loojaRollis, nimeList};


};

export default Looja;
